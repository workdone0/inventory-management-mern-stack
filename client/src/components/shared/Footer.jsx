import React from "react";

import "./Footer.css";

function Footer() {
  return (
    <div className="Footer">
      <span>&copy; 2020 Company Name</span>
    </div>
  );
}

export default Footer;
