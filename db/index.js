const mongoose = require("mongoose");
console.log("URL" + process.env.PROD_MONGODB);
let MONGODB_URI =
  process.env.PROD_MONGODB ||
  "mongodb+srv://workdone0:workdone0@cluster0.kqdpx.mongodb.net/INVENTORY?retryWrites=true&w=majority";

mongoose
  .connect(MONGODB_URI, { useUnifiedTopology: true, useNewUrlParser: true })
  .then(() => {
    console.log("Successfully connected to MongoDB.");
  })
  .catch((e) => {
    console.error("Connection error", e.message);
  });

const db = mongoose.connection;

module.exports = db;
